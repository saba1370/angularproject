import { Component, ViewChild } from '@angular/core';
import { InfoService } from './info.service';
import { History } from './models/history';
import { FormComponent } from './form/form.component';
import { Film } from './models/film';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent {
  currentResult:any
  currentFilm:Film

  category:string
  loading:boolean=false
  title:string = 'angular-project';
  history:Array<History> = []

  @ViewChild(FormComponent, {static: false}) form: FormComponent;

  constructor(private infoService:InfoService){}

  ngOnInit(): void {
  }

  historyClick(category:string, id:number){
    //Will populate form here
    this.form.fillForm(category, id)
  }

  searchEventHandler(query:any){
    this.category = query.category
    this.loading=true
    this.infoService.getResults( query.id, query.category)
      .subscribe((result) => {
        console.log(result)
        this.currentResult = result
          
        this.history.push(new History(query.category, query.id))

        this.loading=false

        let filmUrl = result['films'][0]

        this.infoService.getFilm(filmUrl)
          .subscribe((film) => {
            this.currentFilm = film
          });


      }, error => {
        this.category='error';
        let msg = `Could not find ${query.category} with Id: ${query.id}`
        this.currentResult = {name: msg}

        this.loading=false

      });
  }
}
