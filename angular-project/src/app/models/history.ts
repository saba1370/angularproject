export class History {
    time:Date
    category:string
    id:number

    constructor(category, id){
        this.category=category
        this.id=id
        this.time = new Date()
    }
}
