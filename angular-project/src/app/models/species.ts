// We decided not to use these classes because, for our implmentation, the json (result) given back from the server
// already has the properties named, and we can access it directly from the json (ex.  json.name)
// Though in the future if we wanted each class to have methods, then we would use the classes instead of the json directly
export class Species {

    name:string
    classification:string
    designation:string
    average_height:string
    skin_colors:string

    constructor(){}
}
