// We decided not to use these classes because, for our implmentation, the json (result) given back from the server
// already has the properties named, and we can access it directly from the json (ex.  json.name)
// Though in the future if we wanted each class to have methods, then we would use the classes instead of the json directly
export class Starships {

    name:string
    model:string
    manufacturer:string
    cost_in_credits:number
    length:number

    constructor(){}
}
