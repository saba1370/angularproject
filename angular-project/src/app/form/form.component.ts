import { Component, OnInit, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent implements OnInit {
  options:Array<string> = ['people', 'planets', 'vehicles', 'species', 'starships']
  category:string
  id:number
  @Output() searchEvent = new EventEmitter()

  constructor() { }

  ngOnInit() {
  }

  fillForm(category:string, id:number){
    this.id = id
    this.category = category
  }

  emitSearchEvent(){
    
    let searchData = {
      category: this.category,
      id: this.id
    }
    this.searchEvent.emit(searchData)
  }
}
