import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';
import { People } from './models/people';
import { Planets } from './models/planets';
import { Vehicles } from './models/vehicles';
import { Starships } from './models/starships';
import { Film } from './models/film';


@Injectable({
  providedIn: 'root'
})
export class InfoService {
  url = 'https://swapi.co/api/'

  constructor(private http: HttpClient) { }
  // a custom error handler
  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error.message)
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`)
    }

    // return an observable with a user-facing error message
    return throwError(
      'Something bad happened; please try again later.')
  }

  //We decided to data type it as Object because we want many different types of results (ex. spaceship or people)
  //Instead of a specific class, another way is to make a parent class for all the classes returned, but it is not needed for our case
  getResults(id:number, category:string): Observable<Object>{
    let newUrl:string = `${this.url}${category}/${id}` ;

    return this.http.get<Object>(newUrl)
      // handle any errors
      .pipe(retry(3), // retry a failed request up to 3 times
        catchError(this.handleError)
         // then handle the error
      );
    }

  getFilm(filmUrl:string):Observable<Film>{

    return this.http.get<Film>(filmUrl)
      // handle any errors
      .pipe(retry(3), // retry a failed request up to 3 times
        catchError(this.handleError)
          // then handle the error
      );
    }
  }