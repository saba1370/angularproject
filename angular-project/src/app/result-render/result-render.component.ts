import { Component, OnInit, Input } from '@angular/core';
import { Film } from '../models/film';

@Component({
  selector: 'app-result-render',
  templateUrl: './result-render.component.html',
  styleUrls: ['./result-render.component.css']
})
export class ResultRenderComponent implements OnInit {

  @Input() result:Object
  @Input() category:string
  @Input() film:Film
  
  constructor() { }

  ngOnInit() {
    
  }

}
