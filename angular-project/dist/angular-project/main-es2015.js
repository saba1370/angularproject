(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./$$_lazy_route_resource lazy recursive":
/*!******************************************************!*\
  !*** ./$$_lazy_route_resource lazy namespace object ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/app.component.html":
/*!**************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/app.component.html ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!--The content below is only a placeholder and can be replaced.-->\r\n<div style=\"text-align:center;\">\r\n  <h1>\r\n    Welcome to StarWars Info Searcher!\r\n  </h1>\r\n\r\n  <div style=\"display: inline-block; text-align: left; border: black 2px solid; padding: 10px\">\r\n    <input type=\"checkbox\"  [(ngModel)]='checked' > Show History\r\n    <div *ngIf=\"checked\">\r\n      <ul>\r\n        <li *ngFor='let item of history'><b>[ {{item.time |date:\"hh:mm:ss dd/MM/yyyy\"}} ] Category</b>: {{item.category}} <b> Id</b>: {{item.id}} \r\n          <button (click)='historyClick(item.category, item.id)'>Search Again</button>\r\n        </li>\r\n      </ul>\r\n    </div>\r\n    <hr/>\r\n    <app-form (searchEvent)='searchEventHandler($event)' ></app-form>\r\n    <br/>\r\n\r\n\r\n    <app-result-render *ngIf='currentResult!=null && !loading' [category]='category' [result]='currentResult' [film]='currentFilm' ></app-result-render>\r\n    <h5 *ngIf='loading'>Loading...</h5>\r\n  </div>\r\n</div>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/form/form.component.html":
/*!********************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/form/form.component.html ***!
  \********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<aside>\n    <b>Id: </b> <input type=\"number\" min=\"1\" max=\"5\" [(ngModel)]=\"id\" required #idName='ngModel'>\n        \n    <b>Categories: </b> <select [(ngModel)]=\"category\" required #categoryName='ngModel' >\n                    <option *ngFor = 'let option of options' [value]='option'>{{option}}</option>\n                </select>\n    <button (click)='emitSearchEvent()' [disabled]='!idName.valid || !categoryName.valid'>Go</button> <br/>\n    <span [hidden]='idName.valid || idName.pristine'>Please enter an Id</span>\n    <span [hidden]='categoryName.valid || categoryName.pristine'>Please select a category</span>\n    \n    <hr/>\n</aside>\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/result-render/result-render.component.html":
/*!**************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/result-render/result-render.component.html ***!
  \**************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<aside>\r\n    <!-- If error, display error message or instead display name -->\r\n    <h3>{{category==\"error\"?\"Error: \": \"Name: \"}} {{result.name}}</h3>\r\n    \r\n    <span *ngIf='category==\"people\"'>\r\n        Height: {{result.height}} <br>\r\n        Mass: {{result.mass}} <br>\r\n        Hair color: {{result.hair_color}} <br>\r\n        Skin_color: {{result.skin_color}} <br>\r\n    </span>\r\n    <span *ngIf='category==\"planets\"'>\r\n        Rotation Period: {{result.rotation_period}} <br>\r\n        Orbital Period: {{result.orbital_period}} <br>\r\n        Dimaeter: {{result.diameter}} <br>\r\n        Climate: {{result.climate}} <br>\r\n    </span>\r\n    <span *ngIf='category==\"species\"'>\r\n        Classification: {{result.classification}} <br>\r\n        Designation: {{result.designation}} <br>\r\n        Average Height: {{result.average_height}} <br>\r\n        Skin colors: {{result.skin_colors}} <br>\r\n    </span>\r\n    <span *ngIf='category==\"starships\" || category==\"vehicles\"'>\r\n        Model: {{result.model}} <br>\r\n        Manufacturer: {{result.manufacturer}} <br>\r\n        Cost in credits: {{result.cost_in_credits}} <br>\r\n        Length: {{result.length}} <br>\r\n    </span>\r\n    <span *ngIf='category!=\"error\"'>\r\n        <h3>Film: </h3>\r\n        Title: {{film.title}} <br>\r\n        Episode: {{film.episode_id}} <br>\r\n        Director: {{film.director}} <br>\r\n        Producer: {{film.producer}}\r\n    </span>\r\n\r\n\r\n</aside>"

/***/ }),

/***/ "./src/app/app.component.css":
/*!***********************************!*\
  !*** ./src/app/app.component.css ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2FwcC5jb21wb25lbnQuY3NzIn0= */"

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _info_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./info.service */ "./src/app/info.service.ts");
/* harmony import */ var _models_history__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./models/history */ "./src/app/models/history.ts");
/* harmony import */ var _form_form_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./form/form.component */ "./src/app/form/form.component.ts");





let AppComponent = class AppComponent {
    constructor(infoService) {
        this.infoService = infoService;
        this.loading = false;
        this.title = 'angular-project';
        this.history = [];
    }
    ngOnInit() {
    }
    historyClick(category, id) {
        //Will populate form here
        this.form.fillForm(category, id);
    }
    searchEventHandler(query) {
        this.category = query.category;
        this.loading = true;
        this.infoService.getResults(query.id, query.category)
            .subscribe((result) => {
            console.log(result);
            this.currentResult = result;
            this.history.push(new _models_history__WEBPACK_IMPORTED_MODULE_3__["History"](query.category, query.id));
            this.loading = false;
            let filmUrl = result['films'][0];
            this.infoService.getFilm(filmUrl)
                .subscribe((film) => {
                this.currentFilm = film;
            });
        }, error => {
            this.category = 'error';
            let msg = `Could not find ${query.category} with Id: ${query.id}`;
            this.currentResult = { name: msg };
            this.loading = false;
        });
    }
};
AppComponent.ctorParameters = () => [
    { type: _info_service__WEBPACK_IMPORTED_MODULE_2__["InfoService"] }
];
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])(_form_form_component__WEBPACK_IMPORTED_MODULE_4__["FormComponent"], { static: false })
], AppComponent.prototype, "form", void 0);
AppComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-root',
        template: __webpack_require__(/*! raw-loader!./app.component.html */ "./node_modules/raw-loader/index.js!./src/app/app.component.html"),
        styles: [__webpack_require__(/*! ./app.component.css */ "./src/app/app.component.css")]
    })
], AppComponent);



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm2015/platform-browser.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _result_render_result_render_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./result-render/result-render.component */ "./src/app/result-render/result-render.component.ts");
/* harmony import */ var _form_form_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./form/form.component */ "./src/app/form/form.component.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");








let AppModule = class AppModule {
};
AppModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["NgModule"])({
        declarations: [
            _app_component__WEBPACK_IMPORTED_MODULE_4__["AppComponent"],
            _result_render_result_render_component__WEBPACK_IMPORTED_MODULE_5__["ResultRenderComponent"],
            _form_form_component__WEBPACK_IMPORTED_MODULE_6__["FormComponent"]
        ],
        imports: [
            _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__["BrowserModule"],
            _angular_common_http__WEBPACK_IMPORTED_MODULE_7__["HttpClientModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"]
        ],
        providers: [],
        bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_4__["AppComponent"]]
    })
], AppModule);



/***/ }),

/***/ "./src/app/form/form.component.css":
/*!*****************************************!*\
  !*** ./src/app/form/form.component.css ***!
  \*****************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".ng-valid {\r\n    border-left: 5px solid green;\r\n}\r\n.ng-invalid {\r\n    border-left: 5px solid red;\r\n}\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvZm9ybS9mb3JtLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7SUFDSSw0QkFBNEI7QUFDaEM7QUFDQTtJQUNJLDBCQUEwQjtBQUM5QiIsImZpbGUiOiJzcmMvYXBwL2Zvcm0vZm9ybS5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLm5nLXZhbGlkIHtcclxuICAgIGJvcmRlci1sZWZ0OiA1cHggc29saWQgZ3JlZW47XHJcbn1cclxuLm5nLWludmFsaWQge1xyXG4gICAgYm9yZGVyLWxlZnQ6IDVweCBzb2xpZCByZWQ7XHJcbn0iXX0= */"

/***/ }),

/***/ "./src/app/form/form.component.ts":
/*!****************************************!*\
  !*** ./src/app/form/form.component.ts ***!
  \****************************************/
/*! exports provided: FormComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FormComponent", function() { return FormComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let FormComponent = class FormComponent {
    constructor() {
        this.options = ['people', 'planets', 'vehicles', 'species', 'starships'];
        this.searchEvent = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
    }
    ngOnInit() {
    }
    fillForm(category, id) {
        this.id = id;
        this.category = category;
    }
    emitSearchEvent() {
        let searchData = {
            category: this.category,
            id: this.id
        };
        this.searchEvent.emit(searchData);
    }
};
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])()
], FormComponent.prototype, "searchEvent", void 0);
FormComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-form',
        template: __webpack_require__(/*! raw-loader!./form.component.html */ "./node_modules/raw-loader/index.js!./src/app/form/form.component.html"),
        styles: [__webpack_require__(/*! ./form.component.css */ "./src/app/form/form.component.css")]
    })
], FormComponent);



/***/ }),

/***/ "./src/app/info.service.ts":
/*!*********************************!*\
  !*** ./src/app/info.service.ts ***!
  \*********************************/
/*! exports provided: InfoService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "InfoService", function() { return InfoService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm2015/index.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm2015/operators/index.js");





let InfoService = class InfoService {
    constructor(http) {
        this.http = http;
        this.url = 'https://swapi.co/api/';
    }
    // a custom error handler
    handleError(error) {
        if (error.error instanceof ErrorEvent) {
            // A client-side or network error occurred. Handle it accordingly.
            console.error('An error occurred:', error.error.message);
        }
        else {
            // The backend returned an unsuccessful response code.
            // The response body may contain clues as to what went wrong,
            console.error(`Backend returned code ${error.status}, ` +
                `body was: ${error.error}`);
        }
        // return an observable with a user-facing error message
        return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["throwError"])('Something bad happened; please try again later.');
    }
    //We decided to data type it as Object because we want many different types of results (ex. spaceship or people)
    //Instead of a specific class, another way is to make a parent class for all the classes returned, but it is not needed for our case
    getResults(id, category) {
        let newUrl = `${this.url}${category}/${id}`;
        return this.http.get(newUrl)
            // handle any errors
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["retry"])(3), // retry a failed request up to 3 times
        Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(this.handleError)
        // then handle the error
        );
    }
    getFilm(filmUrl) {
        return this.http.get(filmUrl)
            // handle any errors
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["retry"])(3), // retry a failed request up to 3 times
        Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(this.handleError)
        // then handle the error
        );
    }
};
InfoService.ctorParameters = () => [
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"] }
];
InfoService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    })
], InfoService);



/***/ }),

/***/ "./src/app/models/history.ts":
/*!***********************************!*\
  !*** ./src/app/models/history.ts ***!
  \***********************************/
/*! exports provided: History */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "History", function() { return History; });
class History {
    constructor(category, id) {
        this.category = category;
        this.id = id;
        this.time = new Date();
    }
}
History.ctorParameters = () => [
    null,
    null
];


/***/ }),

/***/ "./src/app/result-render/result-render.component.css":
/*!***********************************************************!*\
  !*** ./src/app/result-render/result-render.component.css ***!
  \***********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3Jlc3VsdC1yZW5kZXIvcmVzdWx0LXJlbmRlci5jb21wb25lbnQuY3NzIn0= */"

/***/ }),

/***/ "./src/app/result-render/result-render.component.ts":
/*!**********************************************************!*\
  !*** ./src/app/result-render/result-render.component.ts ***!
  \**********************************************************/
/*! exports provided: ResultRenderComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ResultRenderComponent", function() { return ResultRenderComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let ResultRenderComponent = class ResultRenderComponent {
    constructor() { }
    ngOnInit() {
    }
};
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])()
], ResultRenderComponent.prototype, "result", void 0);
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])()
], ResultRenderComponent.prototype, "category", void 0);
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])()
], ResultRenderComponent.prototype, "film", void 0);
ResultRenderComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-result-render',
        template: __webpack_require__(/*! raw-loader!./result-render.component.html */ "./node_modules/raw-loader/index.js!./src/app/result-render/result-render.component.html"),
        styles: [__webpack_require__(/*! ./result-render.component.css */ "./src/app/result-render/result-render.component.css")]
    })
], ResultRenderComponent);



/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
const environment = {
    production: false
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm2015/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");




if (_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])
    .catch(err => console.error(err));


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! C:\Users\Administrator\Desktop\Angular Training\angularproject\angular-project\src\main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main-es2015.js.map