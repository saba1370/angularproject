Web Technologies with Angular: Assessed Project
===============================================
There is an API at https://swapi.co/api
You will build a new Angular app which exposes this API to the user
Your completed code should be commited to a public repository 
At the end, you will fill in the usual online form indicating your team members and the repository I can get your development code from

Warning: a few requests don't work, such as https://swapi.co/api/vehicles/3 
(You should code for those outcomes)

What to do
----------
Start a new Angular app. Present the user with a choice of category (people, planets, vehicles, species or starships). Also let them choose a number (1-5) and provide a button.
Clicking the button should call a method of a service, passing in parameters for 'category' and 'number'. 
From the returned data show the 'name' property.
Conditionally show a few other properties, so for example 'planets' have a population and rotation period, while 'species' have height and eye colour.
All form fields are required and where relevant, validated

It is very likey your app will end up with:
- At least one form (template-driven or reactive) with validated fields bound to a model
- At least one service 
- Some TypeScript data-typing, including custom class usage
- At least two child components
- Evidence of @Input() and @Output() and/or route parameters
- Appropriate use of *ngIf and/or [hidden]
- At least one use of *ngFor

There should be some minimal unit testing, e.g.
- Page title is correct
- App is instantiated

Also generate a production version

There is credit for your architectural decisions, such as using components, classes and services, and for tidy, reasonable code. Comments are always welcome
There is no expectation to use routing: use it if it fits your desired solution

There is credit for the look of your app. Things should be clear and layout should be clean
There is no expectation for any css.

Stretch
-------
For each successful search, append the search criteria to an array of historical searches, along with a time-stamp. Provide a way for the user to see this historical search list. Clicking one of the members of the search history should populate the search form with those values, ready to make a fresh search.

All results already include 'film' data. Use a service to make a second request (when the first one returns) retrieving details of the first film. Show some of the film data members.

Completely Optional (no credit)
-------------------------------
DO NOT COMMIT SIGNIFICANT TIME TO THIS
See if you can deploy the production build to a server